Rails.application.routes.draw do
  devise_for :users

  namespace :api do
    resources :health, only: %w[index]

    namespace :v1 do
      resources :boards
      resources :columns
    end
  end
end
