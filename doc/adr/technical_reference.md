- (Rails doc)[https://edgeguides.rubyonrails.org/api_app.html]
- (Trello api doc)[https://developer.atlassian.com/cloud/trello/rest/api-group-actions/]
- (New Rails App with RSPEC, PostgreSQL)[https://medium.com/@cipeinado/how-to-build-a-new-rails-app-with-rspec-postgresql-and-git-1d33c7e60456]
## DB
- Postgresql
## Tests
- Rspec 
- (simplecov)[https://github.com/simplecov-ruby/simplecov]

## Authentication
- (Devise) [https://github.com/heartcombo/devise]
- (Devise-JWT)[https://github.com/waiting-for-dev/devise-jwt]
- Simple Token Authentication [https://github.com/gonzalo-bulnes/simple_token_authentication]
  *Choice for now*: Devise + Simple Token Authentication

## Authorization
- (cancancan)[https://github.com/CanCanCommunity/cancancan]
  *Choice for now*: Simple before action validation

## Used commands:
```
# project creation:
rails new trelllo --api -T -d postgresql


# Postgre dependencies:
sudo apt-get install libpq-dev build-essential

# Generate:
rails g model Board name:string description:text user:references
rails g controller api/v1/boards
rails g model Column name:string description:text board:references
rails g controller api/v1/columns
```