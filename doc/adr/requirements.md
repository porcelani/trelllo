Implement an API that could be used for a project similar to trello.com

## Requirements:
- support CRUD operations for boards / columns / stories
- support ordering of columns / stories
- stories have status and due date and want an API endpoint which will allow us to filter by 1 or more statuses and 1 or more dates
- auth + access control (one user can have no access OR view access OR edit access to a board)

## Mentions:
- has to be built on RoR
- deliverable should be a public git repo with a readme on how to run it 
- will evaluate the code quality | readability | maintainability 