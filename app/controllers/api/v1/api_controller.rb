module Api::V1
  class ApiController < ApplicationController
    acts_as_token_authentication_handler_for User

    before_action :require_authentication!

    rescue_from ActiveRecord::RecordNotFound, with: :not_found
    rescue_from ActiveRecord::RecordInvalid, with: :record_invalid

    private

    def require_authentication!
      throw(:warden, scope: :user) unless current_user.presence
    end

    def not_found
      render json: {
        'errors': [
          {
            'status': '404',
            'title': 'Not Found'
          }
        ]
      }, status: 404
    end

    def record_invalid(message)
      render json: {
        'errors': [
          {
            'status': '400',
            'title': message
          }
        ]
      }, status: 400
    end
  end
end
