class Api::V1::BoardsController < Api::V1::ApiController
  include Authorization
  before_action :set_board, only: %i[show update destroy]

  before_action :require_authorization_to_read!, only: %i[show]
  before_action :require_authorization_to_write!, only: %i[update]
  before_action :require_authorization_to_delete!, only: %i[destroy]

  # GET /api/v1/boards
  def index
    @boards = current_user.boards

    render json: @boards
  end

  # GET /api/v1/boards/1
  def show
    render json: @board
  end

  # POST /api/v1/boards
  def create
    @board = Board.new(board_params.merge(user: current_user))

    if @board.save
      render json: @board, status: :created
    else
      render json: @board.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/boards/1
  def update
    if @board.update(board_params)
      render json: @board
    else
      render json: @board.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/boards/1
  def destroy
    @board.destroy
  end

  private

  def set_board
    @board = Board.find(params[:id])
  end

  def board_params
    params.require(:board).permit(:name,:description)
  end


end
