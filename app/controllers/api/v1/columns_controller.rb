class Api::V1::ColumnsController < Api::V1::ApiController
  include Authorization
  before_action :set_column, only: %i[show update destroy]

  before_action :require_authorization_to_read!, only: %i[show]
  before_action :require_authorization_to_write!, only: %i[update]
  before_action :require_authorization_to_delete!, only: %i[destroy]

  # GET /api/v1/columns/1
  def show
    render json: @column
  end

  # POST /api/v1/columns
  def create
    @column = Column.new(column_params)

    if @column.save
      render json: @column, status: :created
    else
      render json: @column.errors, status: :unprocessable_entity
    end
  end

  private

  def set_column
    @column = Column.find(params[:id])
    @board = @column.board
  end

  def column_params
    params.require(:column).permit(:name, :description, :board_id)
  end
end
