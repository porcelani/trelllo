module Authorization extend ActiveSupport::Concern

  def require_authorization_to_read!
    render json: {}, status: :forbidden unless current_user == @board.user || @board.permission >= Board.permissions[:read]
  end

  def require_authorization_to_write!
    render json: {}, status: :forbidden unless current_user == @board.user || @board.permission >= Board.permissions[:write]
  end

  def require_authorization_to_delete!
    render json: {}, status: :forbidden unless current_user == @board.user
  end
end
