class Board < ApplicationRecord
  belongs_to :user
  has_many :column, dependent: :destroy

  validates :name, :permission, :user, presence: true

  before_save :default_values

  enum permissions: { forbidden: 0, read: 1, write: 2 }

  def default_values
    self.permission ||= permissions[:forbidden]
  end
end
