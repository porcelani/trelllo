## Trelllo

## Requirements:
- ruby -v 2.7.0
- rails -v 6.0.1
- docker-compose

## Executing Locally
```
docker-compose up -d
bundle install
rake db:create
rake db:migrate
rake db:seed
```

## Executing Tests
```
rake spec
```


## API Overview:
### /api/health
```
curl -X GET 
 -H "Content-Type: application/json" \
 http://localhost:3000/api/health
---
200 OK
{"status":"OK"}
```

### /api/v1/boards
```
curl -X POST \
-H "Content-Type: application/json" \
-H "X-User-Email: porcelani@gmail.com" \
-H "X-User-Token: INCLUDE_HERE_AUTHENTICATION_TOKEN" \
--data '{"board": { "name": "Board 1"}}' \
 http://localhost:3000/api/v1/boards
---
201 Created
{"id":1,"name":"Board 1","description":null,"user_id":1,"created_at":"..."updated_at":"..."}
```

```
curl -X GET \
-H "Content-Type: application/json" \
-H "X-User-Email: porcelani@gmail.com" \
-H "X-User-Token: INCLUDE_HERE_AUTHENTICATION_TOKEN" \
 http://localhost:3000/api/v1/boards/1
---
200 OK
{"id":1,"name":"Board 1","description":null,"user_id":1,"created_at":"..."updated_at":"..."}
```


### /api/v1/columns
```
curl -X POST \
-H "Content-Type: application/json" \
-H "X-User-Email: porcelani@gmail.com" \
-H "X-User-Token: INCLUDE_HERE_AUTHENTICATION_TOKEN" \
--data '{"column": { "name": "Column 1","board_id":1}}' \
 http://localhost:3000/api/v1/columns
---
201 Created
{"id":1,"name":"Column 1","description":null,"board_id":1,"created_at":"..."updated_at":"..."}
```
```
curl -X GET \
-H "Content-Type: application/json" \
-H "X-User-Email: porcelani@gmail.com" \
-H "X-User-Token: INCLUDE_HERE_AUTHENTICATION_TOKEN" \
 http://localhost:3000/api/v1/columns/1
---
200 OK
{"id":1,"name":"Column 1","description":null,"board_id":1,"created_at":"..."updated_at":"..."}
```

## References:
- [Documents](doc)
