FactoryBot.define do
  factory :column do
    name { "MyString" }
    description { "MyText" }
    board { nil }
  end
end
