require 'rails_helper'

RSpec.describe 'Api::V1::Columns', type: :request do
  let(:user) { create_user }
  let(:board) { create_board(user) }
  let(:column) { create_column(board) }

  context 'When fetching a column' do
    before do
      get "/api/v1/columns/#{column.id}",
          headers: {
            'X-User-Email': user.email,
            'X-User-Token': user.authentication_token
          }
    end

    it 'returns 200' do
      expect(response.status).to eq(200)
    end

    it 'returns the column' do
      expect(json).to have_id(column.id)
    end
  end

  context 'When the permission is forbidden' do
    let(:other_user) { create_user }
    let(:other_board) { create_board(other_user) }
    let(:other_column) { create_column(other_board) }
    before do
      get "/api/v1/columns/#{other_column.id}",
          headers: {
            'X-User-Email': user.email,
            'X-User-Token': user.authentication_token
          }
    end

    it 'returns 403' do
      expect(response.status).to eq(403)
    end
  end
end
