require 'rails_helper'

RSpec.describe 'Api::V1::Boards', type: :request do
  let(:user) { create_user }
  let(:board) { create_board(user) }

  context 'When fetching a board' do
    before do
      get "/api/v1/boards/#{board.id}",
          headers: {
            'X-User-Email': user.email,
            'X-User-Token': user.authentication_token
          }
    end

    it 'returns 200' do
      expect(response.status).to eq(200)
    end

    it 'returns the board' do
      expect(json).to have_id(board.id)
    end
  end

  context 'When a user is missing' do
    before do
      get '/api/v1/boards/99999999',
          headers: {
            'X-User-Email': user.email,
            'X-User-Token': user.authentication_token
          }
    end

    it 'returns 404' do
      expect(response.status).to eq(404)
    end
  end

  context 'When the Authorization header is missing' do
    before do
      get "/api/v1/boards/#{board.id}"
    end

    it 'returns 302' do
      expect(response.status).to eq(302)
    end
  end

  context 'When the permission is forbidden' do
    let(:other_user) { create_user }
    let(:other_board) { create_board(other_user) }

    before do
      get "/api/v1/boards/#{other_board.id}",
          headers: {
            'X-User-Email': user.email,
            'X-User-Token': user.authentication_token
          }
    end

    it 'returns 403' do
      expect(response.status).to eq(403)
    end
  end

  context 'When the permission is read' do
    let(:other_user) { create_user }
    let(:other_board) { create_board(other_user, Board.permissions[:read]) }

    before do
      get "/api/v1/boards/#{other_board.id}",
          headers: {
            'X-User-Email': user.email,
            'X-User-Token': user.authentication_token
          }
    end

    it 'returns 200' do
      expect(response.status).to eq(200)
    end
  end

  context 'When the permission is write' do
    let(:other_user) { create_user }
    let(:other_board) { create_board(other_user, Board.permissions[:write]) }

    before do
      get "/api/v1/boards/#{other_board.id}",
          headers: {
            'X-User-Email': user.email,
            'X-User-Token': user.authentication_token
          }
    end

    it 'returns 200' do
      expect(response.status).to eq(200)
    end
  end
end
