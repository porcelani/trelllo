require 'rails_helper'

RSpec.describe Board, type: :model do
  let(:user) { create_user }

  subject do
    described_class.new(name: 'Anything',
                        description: 'Lorem ipsum',
                        permission: Board.permissions[:read],
                        user_id: user.id)
  end

  describe 'Validations' do
    it 'is valid' do
      expect(subject).to be_valid
    end
    it 'is not valid without a name' do
      subject.name = nil
      expect(subject).to_not be_valid
    end
    it 'is not valid without a permission' do
      subject.permission = nil
      expect(subject).to_not be_valid
    end
    it 'is not valid without a user' do
      subject.user_id = nil
      expect(subject).to_not be_valid
    end
  end
end
