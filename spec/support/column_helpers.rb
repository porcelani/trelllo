require 'faker'
require 'factory_bot_rails'

module ColumnHelpers
  def build_column
    FactoryBot.build(:column, name: Faker::Name.name, description: Faker::Lorem.sentence)
  end

  def create_column(board)
    FactoryBot.create(:column,
                      name: Faker::Name.name,
                      description: Faker::Lorem.sentence,
                      board: board)
  end
end
