require 'faker'
require 'factory_bot_rails'

module BoardHelpers
  def build_board
    FactoryBot.build(:board, name: Faker::Name.name, description: Faker::Lorem.sentence)
  end

  def create_board(user, permission = Board.permissions[:forbidden])
    FactoryBot.create(:board,
                      name: Faker::Name.name,
                      description: Faker::Lorem.sentence,
                      permission: permission,
                      user: user)
  end
end
